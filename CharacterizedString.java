import java.util.Scanner;

class Characterizedstring {
	public static void main(String[] args){

		String nama;

		Scanner input = new Scanner(System.in);

		System.out.print("Ketik kata/kalimat, lalu tekan enter: ");
		nama = input.nextLine();		
		System.out.println("================================");
		System.out.println("String Penuh: "+nama);
		System.out.println("================================");
		System.out.println("Process memecahkan kata/kalimat menjadi karakter...");
		System.out.println("...........");
		System.out.println("Done.");
		System.out.println("================================");
		for(int i=0;i<nama.length();i++){
			int a=1+i;
			System.out.println("Karakter ["+ a +"]: "+nama.charAt(i));
		}
		System.out.println("================================");
	}
}